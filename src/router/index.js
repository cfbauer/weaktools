import Vue from 'vue'
import VueRouter from 'vue-router'
import Converter from '../components/converter.vue';
import Wilks from '../components/wilks.vue';
import Points from '../components/points.vue';
import About from '../components/about.vue';
// import App from '../App.vue';

Vue.use(VueRouter)

const routes = [
  {
    path: '/Wilks',
    name: 'Wilks',
    component: Wilks
  },
  {
    path: '/',
    name: 'Converter',
    component: Converter
  },
  {
    path: '/points',
    name: 'Points',
    component: Points
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },  
  // {
  //   path: '/',
  //   name: 'App',
  //   component: App
  // }
]

const router = new VueRouter({
  routes
})

export default router
